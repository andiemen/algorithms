'''Given a string s, find the longest palindromic substring in s.
You may assume that the maximum length of s is 1000.'''

def longest_palindrome(s: str) -> str:
    '''Given a string s, find the longest palindromic substring in s.
    You may assume that the maximum length of s is 1000.
    O(N^2)'''
    if len(s) == 0:
        return ''
    if len(s) == 1:
        return s
    res = ''
    for i in range(len(s)):
        for j in range(i + 1, len(s) + 1):
            print(f'i: {i}, j: {j}')
            print(f'{s[i:j]=} | {s[i:j][::-1]=}')
            if s[i:j] == s[i:j][::-1]:
                if len(s[i:j]) > len(res):
                    print(f'longest_palindrome: {s[i:j]}')
                    res = s[i:j]
                    if len(res) == len(s):
                        return res
    return res

def longestPalindrome(s: str) -> str:
    res = ""
    resLen = 0
    len_s = len(s)

    for i in range(len_s):
        # odd length
        l, r = i, i
        while l >= 0 and r < len_s and s[l] == s[r]:
            if(r - l + 1) > resLen:
                res = s[l:r+1]
                resLen = r - l + 1
            l -= 1
            r += 1
        
        # even length
        l, r = i, i + 1
        while l >= 0 and r < len_s and s[l] == s[r]:
            if(r - l + 1) > resLen:
                res = s[l:r+1]
                resLen = r - l + 1
            l -= 1
            r += 1
    return res

if __name__ == '__main__':
    # print(longest_palindrome('babad'))
    # print(longestPalindrome('babad'))
    print(longestPalindrome('bb'))
    print(2 % 2)
    # print(longest_palindrome('cbbd'))
    # print(longest_palindrome('racecar'))
