
def PrintMove(start: int, end: int) -> None:
    '''Prints the move from start to end'''
    print(f'{start} --> {end}\n')

def TowersOfHanoi(n: int, start: int, end: int) -> None:
    '''Solve the towers of Hanoi'''
    if n == 1 : PrintMove(start, end)
    else:
        other = 6 - (start + end) # Gives the location of the other poles
        # 6 because pole (1) + pole (2) + pole (3) = 6
        TowersOfHanoi(n-1, start, other)
        PrintMove(start, end)
        TowersOfHanoi(n-1, other, end)

if __name__ == '__main__':
    TowersOfHanoi(3,1,3)
