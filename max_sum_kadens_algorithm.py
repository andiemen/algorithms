'''Given an array of integers, find the maximum
possible sum you can get from one of its
contiguous subarrays.
The subarray from which this sum comes must
contain at least one element.'''

from typing import List


def max_subarray_sum(arr: List[int]) -> int:
    '''Given an array of integers, find the maximum
    possible sum you can get from one of its
    contiguous subarrays.
    The subarray from which this sum comes must
    contain at least one element.'''
    if len(arr) == 0:
        return 0
    if len(arr) == 1:
        return arr[0]
    max_sum = arr[0]
    current_sum = arr[0]
    for i in range(1, len(arr)):
        current_sum = max(arr[i], current_sum + arr[i])
        max_sum = max(current_sum, max_sum)
    return max_sum

if __name__ == '__main__':
    # sample_arr = [-2, 2, 5, -11, 6] 
    sample_arr = [1, 2, -1, 3, 4, -1]
    print(max_subarray_sum(sample_arr))
