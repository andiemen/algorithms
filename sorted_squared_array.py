'''Input array is sorted
We have to return a sorted squared array.
Some elements could be negative.
N/B Best sorting Algorithm is O(NlogN)'''

from typing import List


def SortedSquaredArray(array: List[int]) -> List[int]:
    '''Squares and Sorts the input array
    Brute force'''
    squared_numbers = []
    for number in array:
        squared_numbers.append(number ** 2)
    squared_numbers.sort()
    return squared_numbers

def SortedSquaredArrayOptimised(array: List[int]) -> List[int]:
    '''Using the fact that the given array is sorted'''
    length_of_array = len(array)
    result = [0 for i in range(length_of_array)] # Create an equal sized array
    left = 0
    right = length_of_array - 1
    for i in range(length_of_array):
        current_reverse_index = length_of_array - i -1 # 
        if abs(array[left]) > array[right]:
            result[current_reverse_index] = array[left] ** 2
            left += 1
        else:
            result[current_reverse_index] = array[right] ** 2
            right -= 1
    return result


if __name__ == '__main__':
    sample_array = [-6, -4, 1, 2, 3, 5] # Expected [1, 4, 9, 16, 25, 36]
    print(SortedSquaredArray(sample_array))
    print(SortedSquaredArrayOptimised(sample_array))
