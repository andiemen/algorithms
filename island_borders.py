def is_border(i, row_length, j, col_length):
    if i == 0 or i == row_length or j == 0 or j == col_length:
        return True
    return False

def is_outside_matrix(i, j, matrix):
    if i < 0 or j < 0 or i > len(matrix) - 1 or j > len(matrix[0]) - 1:
        return True
    return False

def border_island_key(i, j):
    return f'{i}{j}'

def rec(i, j, matrix, border_islands):
    steps = [
        (0, 1),
        (1, 0),
        (0, -1),
        (-1, 0),
    ]
    for (ix, jx) in steps:
        new_i = ix + i
        new_j = jx + j
        if is_outside_matrix(new_i, new_j, matrix):
            continue
        neigh = matrix[new_i][new_j]
        if neigh == 1:
            key = border_island_key(new_i, new_j)
        border_islands[key] = True
        if not (key in border_islands):
            rec(new_i, new_j, matrix, border_islands)

def removeIslands(matrix):
    pass
