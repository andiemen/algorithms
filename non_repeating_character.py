'''Find first non repaeting character in an array 
of characters return _ if not found'''

#BruteForce Method
def FirstNotRepeatingCharacter(in_string: str) -> str:
    '''Creates two pointers, fixes one and seraches 
    for its duplicate accross the rest of the string
    time = O(N^2)'''
    for i in range(len(in_string)):
      seen_duplicate = False
      for j in range(len(in_string)):
        # print(f'i: {in_string[i]} {i}, j: {in_string[j]} {j}')
        if in_string[i] == in_string[j] and i != j:
          seen_duplicate = True
          break
      if not seen_duplicate: 
        return in_string[i]
    return '_'

#Using HashMap
def FNRC_Hashmap(in_string: str) -> str:
    '''Uses a dict(HashMap) to get the 
    First Non-Repeating Character
    time = O(2N) = O(N)'''
    char_counts = {}
    for character in in_string:
        char_counts[character] = char_counts.get(character, 0) + 1
    
    for k, v in char_counts.items():
        if v == 1:
            return k
    return '_'

#Using built in str index
def FNRC_Index(in_string: str) -> str:
    '''Use built in functions to find the 
    First Non-Repeating Character
    time = O(N)'''
    for character in in_string:
        if in_string.index(character) == in_string.rindex(character):
        # If the first occurance is the last occurance
            return character
    return '_'

if __name__ == '__main__':
    # print(FirstNotRepeatingCharacter('aaabcdcceef'))
    # print(FNRC_Hashmap('aaabcdcceef'))
    print(FNRC_Index('aaabcdcceef'))
