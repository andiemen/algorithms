'''Find the longest consecutive 
sequence in an array of integers.'''

from typing import List


def longest_consecutive_seq_n2(arr: List[int]) -> int:
    '''Find the longest consecutive 
    sequence in an array of integers.'''
    if len(arr) == 0:
        return 0
    max_sequence_length = 0
    for num in arr:
        current_sequence_length = 1
        while num + 1 in arr:
            current_sequence_length += 1
            num += 1
        max_sequence_length = max(max_sequence_length, current_sequence_length)
    return max_sequence_length

def longest_consecutive_seq_n(arr: List[int]) -> int:
    '''Find the longest consecutive 
    sequence in an array of integers.'''
    if len(arr) == 0:
        return 0
    max_sequence_length = 0
    for num in arr:
        current_sequence_length = 1
        if num - 1 in arr:
            continue
        while num + 1 in arr:
            current_sequence_length += 1
            num += 1
        max_sequence_length = max(max_sequence_length, current_sequence_length)
    return max_sequence_length

if __name__ == '__main__':
    sample_array = [100, 4, 200, 1, 3, 2]
    # print(longest_consecutive_seq_n2(sample_array))
    # print(longest_consecutive_seq_n(sample_array))
