'''
You are given two *non-empty* linked lists representing two
*non-negative* integers. The digits are stored in reverse order,
and each of their nodes contains a single digit.
Add the two numbers and return the sum as a linked list.

You may assume the two numbers do not contain any leading zero,
except the number 0 itself.

Example
Input: l1 = [2,4,3], l2 = [5,6,4]
Output: [7,0,8]
Explanation: 342 + 465 = 807.

Input: l1 = [0], l2 = [0]
Output: [0]

Input: l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9]
Output: [8,9,9,9,0,0,0,1]

Input: l1 = [8], l2 = [7]
Output: [5,1]

https://leetcode.com/problems/add-two-numbers/
'''

# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, val=0, next_node=None):
        self.val = val
        self.next_node = next_node

class Solution(object):
    def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """
        dummy = ListNode()
        cur = dummy

        carry = 0
        while l1 or l2 or carry:
            v1 = l1.val if l1 else 0
            v2 = l2.val if l2 else 0

            # new digit
            val = v1 + v2 + carry
            carry = val // 10
            val = val % 10
            cur.next_node = ListNode(val)

            # update pointers
            cur = cur.next_node
            l1 = l1.next_node if l1 else None
            l2 = l2.next_node if l2 else None

        return dummy.next_node
