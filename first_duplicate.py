'''Get the Firt recurring element of a list'''

#BruteForce
def FirstDuplicate(elements: list) -> int:
    '''Returns the first duplicate.
    ie The second occurance of this duplicate
    come before and other duplicate occurance.
    [1,2,1,2,3,3] -> 1
    [2,1,3,5,3,2] -> 3
    [1,2,3,4,5,6] -> -1
    time = O(N^2)'''
    len_of_input = len(elements)
    min_indx_of_duplicate = len_of_input
    for i in range(len_of_input):
        for j in range(i+1, len_of_input):
            if elements[i] == elements[j]:
                min_indx_of_duplicate = min(min_indx_of_duplicate, j)
    if min_indx_of_duplicate == len_of_input:
        return -1
    else:
        return elements[min_indx_of_duplicate]

#Using Set
def FirstDuplicate_Set(elements: list) -> int:
    '''Uses a Set to get the 
    First Duplicate
    [1,2,1,2,3,3] -> 1
    [2,1,3,5,3,2] -> 3
    [1,2,3,4,5,6] -> -1
    time = O(N)'''
    seen = set() # Additional Space Complexity
    for element in elements:
        if element in seen:
            return element
        else: 
            seen.add(element)
    
    return -1

#Using less space
def FirstDuplicate_Space(elements: list) -> int:
    '''Uses less space to get the 
    First Duplicate
    [1,2,1,2,3,3] -> 1
    [2,1,3,5,3,2] -> 3
    [1,2,3,4,5,6] -> -1
    time = O(N)'''
    for i in range(len(elements)):
        abs_current_value = abs(elements[i])
        if elements[abs_current_value-1] < 0:
            return abs_current_value
        else:
            elements[abs_current_value-1] *= -1
    
    return -1


if __name__ == '__main__':
    sample_array = [2,1,3,5,3,2]
    # print(FirstDuplicate([2,1,3,5,3,2]))
    # print(FirstDuplicate_Set(sample_array))
    print(FirstDuplicate_Space(sample_array))
    print(f"{sample_array=}")
